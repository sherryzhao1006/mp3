//
//  ViewController.m
//  Connect4
//
//  Created by Michael Lee on 4/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "ViewController.h"
#import "GameModel.h"

@interface ViewController ()
- (void)reinitGame;
@end

@implementation ViewController {
    GameModel *gameModel;
    NSMutableArray *pieces;
    int winner;
    NSTimer *timer;
    int a;
    int numberOfTaps[7];
    bool find;

}
- (void)awakeFromNib
{
    pieces = [NSMutableArray array];
    gameModel = [[GameModel alloc] init];
}
-(void)viewDidLoad{
    [super viewDidLoad];
    [self.countDownTimer setText:@"20"];
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    find=NO;
    self.player1.backgroundColor=[UIColor redColor];
    self.player2.backgroundColor=[UIColor yellowColor];
    [self.player1.layer setCornerRadius:23.5];
    [self.player2.layer setCornerRadius:23.5];
    gameModel=[[GameModel alloc] init];
    //PlayerNO=1;
    self.boardView = [[BoardView alloc] initWithFrame:self.view.bounds slotDiameter:40];
    self.boardView.delegate = self;
    [self.view addSubview:self.boardView];
    [self.view bringSubviewToFront:self.restartbutton];

}

- (void)reinitGame
{
    [gameModel resetGame];
    winner=0;
    for (UIView *view in pieces) {
        [view removeFromSuperview];
    }
    [pieces removeAllObjects];
}

- (void)boardView:(BoardView *)boardView columnSelected:(int)column
{
    if ([gameModel processTurnAtCol:column]) {
        if ([gameModel isGameOver]) {
            NSLog(@"Game Over!, Player %d won", [gameModel winner]);
            winner=[gameModel winner];
            //NSLog(@"%d",winner);
        } else {
           // NSLog(@"col:%d",column-1);
            int row = [gameModel topRowInCol:column] - 1;
           // NSLog(@"row:%d",row);
            UIView *piece = [[UIView alloc]
                             initWithFrame:CGRectMake(100+column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                      -self.boardView.slotDiameter,
                                                      self.boardView.slotDiameter,
                                                      self.boardView.slotDiameter)];
            [pieces addObject:piece];
            piece.backgroundColor = [gameModel colorForCurrentTurn];
            [self.view insertSubview:piece belowSubview:self.boardView];
            [UIView animateWithDuration:0.5 animations:^{
                piece.center = CGPointMake(100+column*self.boardView.gridWidth,
                                           (6 - row)*self.boardView.gridHeight);
            }];
        }
    }
    numberOfTaps[column-1]++;

    NSLog(@"winner is:%d",winner);
            if (winner==1) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Game Over" message:@"Player 1 Win!" delegate:self cancelButtonTitle:@"Restart Game" otherButtonTitles:@"Cancel", nil];
                [alert show];
                find=YES;

            }
            if (winner==2) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Game Over" message:@"Player 2 Win!" delegate:self cancelButtonTitle:@"Restart Game" otherButtonTitles:@"Cancel", nil];
                [alert show];
                find=YES;

            }
    if (!find) {
        a=0;
        [timer invalidate];
        timer=nil;
        timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
        [self.countDownTimer setText:@"20"];
    }


}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        [self reinitGame];
    }
}
-(void)timerFired{
    int timeLeft=[self.countDownTimer.text intValue];
    if (timeLeft>0) {
        self.countDownTimer.text=[NSString stringWithFormat:@"%d",timeLeft-1];
    }else{
        
        while (YES) {
            int randomColumn=arc4random()%7;
            [self boardView:self.boardView columnSelected:randomColumn+1];
            UIView *piece = [[UIView alloc]
                             initWithFrame:CGRectMake(100+randomColumn*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                      -self.boardView.slotDiameter,
                                                      self.boardView.slotDiameter,
                                                      self.boardView.slotDiameter)];
            [pieces addObject:piece];
            piece.backgroundColor = [gameModel colorForCurrentTurn];
            [self.view insertSubview:piece belowSubview:self.boardView];

                break;
            
            
        }
    }
}

- (IBAction)restartgame:(id)sender {
    [self reinitGame];
}
@end
