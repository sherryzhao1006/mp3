//
//  ViewController.h
//  Connect4
//
//  Created by Michael Lee on 4/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardView.h"

@interface ViewController : UIViewController <BoardViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *player1;
@property (strong, nonatomic) IBOutlet UILabel *countDownTimer;
@property (strong, nonatomic) IBOutlet UILabel *player2;
@property (strong, nonatomic) BoardView *boardView;
- (IBAction)restartgame:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *restartbutton;
@end
