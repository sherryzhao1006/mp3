//
//  GameInfo.h
//  Connect4
//
//  Created by Michael Lee on 5/1/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameModel : NSObject
@property (readonly) int turn;

- (void)resetGame;
- (UIColor *)colorForCurrentTurn;
- (BOOL)processTurnAtCol:(int)col; // return YES if turn is legal, NO otherwise
- (int)topRowInCol:(int)col;
- (BOOL)isGameOver;
- (int)winner;

@end

