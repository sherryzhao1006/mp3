//
//  GameInfo.m
//  Connect4
//
//  Created by Michael Lee on 5/1/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "GameModel.h"

@interface GameModel ()
- (BOOL)checkForWinAtRow:(int)row col:(int)col;
@end

@implementation GameModel {
    int _winner;
    int _turn;
    int _board[6][7];
    int _piecesInCol[7];
    int count;
}

- (id)init
{
    if (self = [super init]) {
        _turn = 1;
        _winner = 0;
        for (int i=0; i<7; i++) {
            _piecesInCol[i] = 0;
        }
       // _piecesInCol[6]=1;
    }
    return self;
}

- (void)resetGame
{
    _turn = 1;
    _winner=0;
    count=0;
    for (int i=0; i<7; i++) {
        _piecesInCol[i] = 0;
    }
    for (int i=0; i<6; i++) {
        for (int j=0; j<7; j++) {
            _board[i][j]=0;
        }
    }
    NSLog(@"%d",_winner);
}

- (int)turn
{
    return _turn;
}

- (UIColor *)colorForCurrentTurn
{
    if (_turn == 1) {
        return [UIColor yellowColor];
    } else {
        return [UIColor redColor];
    }
}

- (BOOL)checkForWinAtRow
{
    //_board[row][col]=_turn;
     count=0;
    //check row
    for (int i=5; i>=0; i--) {//for row
        for (int j=0; j<6; j++) {//for column
            if (_board[i][j]!=0 && _board[i][j]==_board[i][j+1]) {
                count++;
            }
            else
                count=0;
            if (count==3) {
                _winner=_board[i][j];
                count=0;
                return _winner;
                break;
            }
            
        }
    }
    //check column
    count=0;
    for (int j=0; j<7; j++) {//for column
        for (int i=5; i>0; i--) {
            if (_board[i][j]!=0 && _board[i][j]==_board[i-1][j]) {
                count++;
            }
            else count=0;
            if (count==3) {
                _winner=_board[i][j];
                count=0;
                return _winner;
            }
        }
    }
    //check digonal
    count=0;
    for (int i=0; i<3; i++) {
        for (int j=0; j<4; j++) {
            if (_board[i][j]==_board[i+1][j+1]&&_board[i+1][j+1]==_board[i+2][j+2]&&_board[i+2][j+2]==_board[i+3][j+3]) {
                if (_board[i][j]!=0) {
                    _winner=_board[i][j];
                }
            }
        }
    }
    for (int i=0; i<3; i++) {
        for (int j=3; j<7; j++) {
            if (_board[i][j]==_board[i+1][j-1]&&_board[i+1][j-1]==_board[i+2][j-2]&&_board[i+2][j-2]==_board[i+3][j-3]) {
                if (_board[i][j]!=0) {
                    _winner=_board[i][j];
                }
            }
        }
    }
    if (_winner!=0) {
        NSLog(@"winner is:%d",_winner);

        return YES;}
    else return NO;
    //return _winner;
}

- (BOOL)isGameOver
{
    return (_winner != 0);
}

- (int)winner
{
    return _winner;
}

- (BOOL)processTurnAtCol:(int)col
{
    if ([self isGameOver]) {
        return NO;
    }
    
    if ([self topRowInCol:col] <= 6) {
        _board[_piecesInCol[col]][col] = _turn; // track move
        _piecesInCol[col]++;
        if ([self checkForWinAtRow]) {
            _winner = _turn;
        }
        _turn = (_turn == 1)? 2 : 1;
        return YES;
    } else {
        return NO;
    }
}

- (int)topRowInCol:(int)col
{
    NSLog(@"_piecesInCol:%d",_piecesInCol[col]);
    
    return _piecesInCol[col];
}
@end
